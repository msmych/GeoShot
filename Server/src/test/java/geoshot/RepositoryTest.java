package geoshot;

import geoshot.bot.user.BotUser;
import geoshot.bot.user.BotUser.Action;
import geoshot.bot.user.BotUser.Role;
import geoshot.bot.user.BotUserRepository;
import geoshot.contribution.Contribution;
import geoshot.contribution.ContributionRepository;
import geoshot.contribution.tag.Tag;
import geoshot.contribution.tag.TagRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static geoshot.bot.user.BotUser.Action.NONE;
import static geoshot.bot.user.BotUser.Role.MODERATOR;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RepositoryTest {

    @Autowired BotUserRepository botUserRepository;
    @Autowired TagRepository tagRepository;
    @Autowired ContributionRepository contributionRepository;

    Tag tag1 = new Tag("tag1");
    Tag tag2 = new Tag("tag2");
    Contribution contribution1 = new Contribution();
    Contribution contribution2 = new Contribution();
    Contribution contribution3 = new Contribution();

    @Before
    public void setUp() {
        BotUser botUser = new BotUser();
        botUser.id = 1;
        botUser.role = MODERATOR;
        botUserRepository.save(botUser);

        tagRepository.saveAll(asList(tag1, tag2));
        contribution1.tag = tag1;
        contribution1.latitude = 1;
        contribution1.longitude = 10;
        contribution2.tag = tag1;
        contribution2.latitude = 20;
        contribution2.longitude = 5;
        contribution3.tag = tag2;
        contribution3.latitude = 1;
        contribution3.longitude = 1;
        contributionRepository.saveAll(asList(contribution1, contribution2, contribution3));
    }

    @Test
    public void selectingActionByBotUserId() {
        Optional<Action> action = botUserRepository.findActionById(1);
        assertTrue(action.isPresent());
        assertEquals(NONE, action.get());
    }

    @Test
    public void selectingRoleByBotUserId() {
        Optional<Role> role = botUserRepository.findRoleById(1);
        assertTrue(role.isPresent());
        assertEquals(MODERATOR, role.get());
    }

    @Test
    public void findingClosest() {
        Optional<Contribution> oc = contributionRepository.findClosest(tag1.id, 5, 5);
        assertTrue(oc.isPresent());
        Contribution contribution = oc.get();
        assertEquals(contribution1.id, contribution.id);
    }
}
