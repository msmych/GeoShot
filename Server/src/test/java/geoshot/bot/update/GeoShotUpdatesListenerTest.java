package geoshot.bot.update;

import com.pengrad.telegrambot.model.Update;
import geoshot.bot.update.GeoShotUpdatesListener.*;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.util.*;

import static com.pengrad.telegrambot.UpdatesListener.CONFIRMED_UPDATES_ALL;
import static java.util.Collections.emptySet;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class GeoShotUpdatesListenerTest {

    Update update = mock(Update.class);
    List<Update> updates = Arrays.asList(update, update, update, update);

    @Test
    public void runingAllPreProcessors() {
        UpdatePreProcessor pp1 = mock(UpdatePreProcessor.class);
        UpdatePreProcessor pp2 = mock(UpdatePreProcessor.class);
        GeoShotUpdatesListener ul = new GeoShotUpdatesListener(new HashSet<>(Arrays.asList(pp1, pp2)), emptySet());
        assertEquals(CONFIRMED_UPDATES_ALL, ul.process(updates));
        verify(pp1, times(updates.size())).preProcess(update);
        verify(pp2, times(updates.size())).preProcess(update);
    }

    @Test
    public void runningSatisfiedNotRunningUnsatisfied() {
        UpdateProcessor applied = mock(UpdateProcessor.class);
        UpdateProcessor notApplied = mock(UpdateProcessor.class);
        when(applied.appliesTo(ArgumentMatchers.isA(Update.class))).thenReturn(true);
        when(notApplied.appliesTo(ArgumentMatchers.isA(Update.class))).thenReturn(false);
        GeoShotUpdatesListener ul = new GeoShotUpdatesListener(
                emptySet(),
                new HashSet<>(Arrays.asList(applied, notApplied)));
        assertEquals(CONFIRMED_UPDATES_ALL, ul.process(updates));
        verify(applied, times(updates.size())).process(update);
        verify(notApplied, times(0)).process(update);
    }

    @Test
    public void catchingAnyException() {
        RuntimeException re = new RuntimeException();
        UpdatePreProcessor throwingWhilePreProcessing = mock(UpdatePreProcessor.class);
        doThrow(re).when(throwingWhilePreProcessing).preProcess(ArgumentMatchers.isA(Update.class));
        UpdateProcessor throwingWhileChecking = mock(UpdateProcessor.class);
        when(throwingWhileChecking.appliesTo(ArgumentMatchers.isA(Update.class))).thenThrow(re);
        UpdateProcessor throwingWhileProcessing = mock(UpdateProcessor.class);
        when(throwingWhileProcessing.appliesTo(ArgumentMatchers.isA(Update.class))).thenReturn(true);
        doThrow(re).when(throwingWhileProcessing).process(ArgumentMatchers.isA(Update.class));
        GeoShotUpdatesListener ul = new GeoShotUpdatesListener(
                Collections.singleton(throwingWhilePreProcessing),
                new HashSet<>(Arrays.asList(throwingWhileChecking, throwingWhileProcessing)));
        assertEquals(CONFIRMED_UPDATES_ALL, ul.process(updates));
    }
}