package geoshot.bot.update;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Location;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import org.junit.Test;

import static geoshot.bot.update.UpdateUtils.isCallbackQueryData;
import static geoshot.bot.update.UpdateUtils.isLocation;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UpdateUtilsTest {

    Update update = mock(Update.class);
    CallbackQuery callbackQuery = mock(CallbackQuery.class);
    Message message = mock(Message.class);

    @Test
    public void callbackQueryData() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.data()).thenReturn("data");
        assertTrue(isCallbackQueryData("data", update));
    }

    @Test
    public void noCallbackQuery() {
        when(update.callbackQuery()).thenReturn(null);
        assertFalse(isCallbackQueryData("data", update));
    }

    @Test
    public void noCallbackQueryData() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.data()).thenReturn(null);
        assertFalse(isCallbackQueryData("data", update));
    }

    @Test
    public void noMessage() {
        when(update.message()).thenReturn(null);
        assertFalse(isLocation(update));
    }

    @Test
    public void noLocation() {
        when(update.message()).thenReturn(message);
        when(message.location()).thenReturn(null);
        assertFalse(isLocation(update));
    }

    @Test
    public void location() {
        when(update.message()).thenReturn(message);
        when(message.location()).thenReturn(mock(Location.class));
        assertTrue(isLocation(update));
    }
}