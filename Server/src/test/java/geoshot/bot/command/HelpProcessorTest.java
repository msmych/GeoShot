package geoshot.bot.command;

import com.pengrad.telegrambot.model.Chat;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import geoshot.bot.resource.IOService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.core.io.Resource;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class HelpProcessorTest {

    Bot bot = mock(Bot.class);
    HelpProcessor hp = new HelpProcessor(bot, mock(Resource.class), mock(IOService.class));

    Update update = mock(Update.class);
    Message message = mock(Message.class);
    Chat chat = mock(Chat.class);
    User user = mock(User.class);

    @Test
    public void helpApplies() {
        when(update.message()).thenReturn(message);
        when(message.text()).thenReturn("/help");
        assertTrue(hp.appliesTo(update));
    }

    @Test
    public void noMessageNotApplies() {
        when(update.message()).thenReturn(null);
        assertFalse(hp.appliesTo(update));
    }

    @Test
    public void noMessageTextNotApplies() {
        when(update.message()).thenReturn(message);
        when(message.text()).thenReturn(null);
        assertFalse(hp.appliesTo(update));
    }

    @Test
    public void helpAtBotNameApplies() {
        when(update.message()).thenReturn(message);
        when(message.text()).thenReturn("/help@BotUsername");
        when(user.username()).thenReturn("BotUsername");
        when(bot.getUser()).thenReturn(user);
        assertTrue(hp.appliesTo(update));
    }

    @Test
    public void sendingMessage() {
        when(update.message()).thenReturn(message);
        when(message.chat()).thenReturn(chat);
        when(chat.id()).thenReturn(1L);
        hp.process(update);
        verify(bot).execute(ArgumentMatchers.isA(SendMessage.class));
    }

}