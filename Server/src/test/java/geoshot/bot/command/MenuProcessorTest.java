package geoshot.bot.command;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.contribution.ContributionService;
import geoshot.bot.user.BotUserService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import static geoshot.bot.user.BotUser.Action.NONE;
import static org.mockito.Mockito.*;

public class MenuProcessorTest {

    BotUserService bus = mock(BotUserService.class);
    ContributionService cs = mock(ContributionService.class);
    Bot bot = mock(Bot.class);
    MenuProcessor mp = new MenuProcessor(bus, cs, bot);
    Update update = mock(Update.class);
    User user = mock(User.class);
    Message message = mock(Message.class);
    CallbackQuery callbackQuery = mock(CallbackQuery.class);

    @Test
    public void editingMessage() {
        int id = 1;
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.from()).thenReturn(user);
        when(user.id()).thenReturn(id);
        when(callbackQuery.message()).thenReturn(message);
        when(message.messageId()).thenReturn(1);
        mp.process(update);
        verify(bus).updateAction(id, NONE);
        verify(cs).cancelSubmissionBy(id);
        verify(bot).execute(ArgumentMatchers.isA(EditMessageText.class));
    }

}