package geoshot.bot.command;

import com.pengrad.telegrambot.model.Chat;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class StartProcessorTest {

    Bot bot = mock(Bot.class);
    StartProcessor sp = new StartProcessor(bot);

    Update update = mock(Update.class);
    Message message = mock(Message.class);
    Chat chat = mock(Chat.class);
    User user = mock(User.class);

    @Test
    public void startApplies() {
        when(update.message()).thenReturn(message);
        when(message.text()).thenReturn("/start");
        assertTrue(sp.appliesTo(update));
    }

    @Test
    public void noMessageNotApplies() {
        when(update.message()).thenReturn(null);
        assertFalse(sp.appliesTo(update));
    }

    @Test
    public void noMessageTextNotApplies() {
        when(update.message()).thenReturn(message);
        when(message.text()).thenReturn(null);
        assertFalse(sp.appliesTo(update));
    }

    @Test
    public void startAtBotNameApplies() {
        when(update.message()).thenReturn(message);
        when(message.text()).thenReturn("/start@BotUsername");
        when(user.username()).thenReturn("BotUsername");
        when(bot.getUser()).thenReturn(user);
        assertTrue(sp.appliesTo(update));
    }

    @Test
    public void sendingMessage() {
        when(update.message()).thenReturn(message);
        when(message.chat()).thenReturn(chat);
        when(chat.id()).thenReturn(1L);
        sp.process(update);
        verify(bot).execute(ArgumentMatchers.isA(SendMessage.class));
    }

}