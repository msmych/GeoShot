package geoshot.bot.user;

import com.pengrad.telegrambot.model.Contact;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ModeratorSavingProcessorTest {

    private BotUserService bus = mock(BotUserService.class);
    private Bot bot = mock(Bot.class);
    ModeratorSavingProcessor msp = new ModeratorSavingProcessor(bus, bot);
    Update update = mock(Update.class);
    Message message = mock(Message.class);
    Contact contact = mock(Contact.class);
    User user = mock(User.class);

    @Before
    public void setUp() {
        when(update.message()).thenReturn(message);
        when(message.contact()).thenReturn(contact);
        when(contact.userId()).thenReturn(1);
        when(message.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        when(bus.isModerator(anyInt())).thenReturn(true);
    }

    @Test
    public void isModeratorAndContactApplies() {
        assertTrue(msp.appliesTo(update));
    }

    @Test
    public void noContactNotApplies() {
        when(message.contact()).thenReturn(null);
        assertFalse(msp.appliesTo(update));
    }

    @Test
    public void noMessageNotApplies() {
        when(update.message()).thenReturn(null);
        assertFalse(msp.appliesTo(update));
    }

    @Test
    public void isNotModeratorNotApplies() {
        when(bus.isModerator(anyInt())).thenReturn(false);
        assertFalse(msp.appliesTo(update));
    }

    @Test
    public void savingModeratorAndSendingMessage() {
        msp.process(update);
        verify(bus).save(isA(Contact.class));
        verify(bot).execute(isA(SendMessage.class));
    }

}