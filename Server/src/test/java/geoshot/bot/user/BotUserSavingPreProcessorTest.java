package geoshot.bot.user;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import static org.mockito.Mockito.*;

public class BotUserSavingPreProcessorTest {

    BotUserService service = mock(BotUserService.class);
    BotUserSavingPreProcessor pp = new BotUserSavingPreProcessor(service);
    Update update = mock(Update.class);
    Message message = mock(Message.class);
    User user = mock(User.class);

    @Test
    public void savingUser() {
        when(update.message()).thenReturn(message);
        when(message.from()).thenReturn(user);
        when(service.save(ArgumentMatchers.isA(User.class))).thenReturn(new BotUser());
        pp.preProcess(update);
        verify(service).save(user);
    }

    @Test
    public void savingUserNoMessage() {
        when(update.message()).thenReturn(null);
        pp.preProcess(update);
    }

}