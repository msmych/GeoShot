package geoshot.bot.user;

import com.pengrad.telegrambot.model.Contact;
import com.pengrad.telegrambot.model.User;
import geoshot.bot.GeoShotBotException;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static geoshot.bot.user.BotUser.Action.CONTRIBUTING1;
import static geoshot.bot.user.BotUser.Role.MODERATOR;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class BotUserServiceTest {

    BotUserRepository bur = mock(BotUserRepository.class);
    List<String> moderators = Arrays.asList("1", "2", "3");
    BotUserService bus = new BotUserService(bur, moderators);
    User user = mock(User.class);
    Contact contact = mock(Contact.class);

    @Test
    public void savingModerators() {
        bus.init();
        verify(bur, times(moderators.size())).save(isA(BotUser.class));
    }

    @Test
    public void savingNewUser() {
        when(user.id()).thenReturn(1);
        BotUser botUser = new BotUser(user);
        when(bur.save(isA(BotUser.class))).then(ia -> ia.getArgument(0));
        assertBotUser(botUser, bus.save(user));
    }

    @Test
    public void savingExistingUser() {
        int botUserId = 1;
        BotUser botUser = new BotUser() {{
            id = botUserId;
            role = MODERATOR;
            action = CONTRIBUTING1;
        }};
        when(user.id()).thenReturn(botUserId);
        when(bur.findById(ArgumentMatchers.anyInt())).thenReturn(of(botUser));
        when(bur.save(isA(BotUser.class))).then(ia -> ia.getArgument(0));
        assertBotUser(botUser, bus.save(user));
    }

    @Test
    public void savingNewContactAsModerator() {
        when(contact.userId()).thenReturn(1);
        BotUser botUser = new BotUser(contact);
        when(bur.findById(anyInt())).thenReturn(empty());
        when(bur.save(isA(BotUser.class))).then(ia -> ia.getArgument(0));
        assertBotUser(botUser, bus.save(contact));
    }

    @Test
    public void savingExistingContactAsModerator() {
        int botUserId = 1;
        when(contact.userId()).thenReturn(botUserId);
        BotUser moderator = new BotUser() {{
            id = botUserId;
            role = MODERATOR;
        }};
        when(bur.findById(anyInt())).thenReturn(of(new BotUser() {{ id = botUserId; }}));
        when(bur.save(isA(BotUser.class))).then(ia -> ia.getArgument(0));
        assertBotUser(moderator, bus.save(contact));
    }

    private void assertBotUser(BotUser expected, BotUser actual) {
        assertEquals(expected.id, actual.id);
        assertEquals(expected.role, actual.role);
        assertEquals(expected.action, actual.action);
    }

    @Test
    public void updatingAction() {
        int botUserId = 1;
        when(bur.findById(botUserId)).thenReturn(of(new BotUser() {{ id = botUserId; }}));
        bus.updateAction(botUserId, CONTRIBUTING1);
        verify(bur).findById(botUserId);
        verify(bur).save(isA(BotUser.class));
    }

    @Test(expected = GeoShotBotException.class)
    public void throwingWhileUpdatingActionIfBotUserNotFound() {
        int botUserId = 1;
        when(bur.findById(botUserId)).thenReturn(Optional.empty());
        bus.updateAction(botUserId, CONTRIBUTING1);
    }

    @Test
    public void checkingIfModerator() {
        when(bur.findRoleById(anyInt())).thenReturn(of(MODERATOR));
        assertTrue(bus.isModerator(1));
    }

    @Test
    public void updatingTargetTag() {
        when(bur.findById(anyInt())).thenReturn(of(new BotUser()));
        bus.updateTargetTag(1, 1);
        verify(bur).findById(anyInt());
        verify(bur).save(isA(BotUser.class));
    }

    @Test
    public void dismissingTargetTag() {
        when(bur.findById(anyInt())).thenReturn(of(new BotUser()));
        bus.dismissSearchingForTag(1);
        verify(bur).findById(anyInt());
        verify(bur).save(isA(BotUser.class));
    }

}