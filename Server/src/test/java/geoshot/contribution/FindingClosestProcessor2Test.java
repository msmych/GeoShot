package geoshot.contribution;

import com.pengrad.telegrambot.model.Location;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.SendLocation;
import com.pengrad.telegrambot.request.SendPhoto;
import geoshot.bot.Bot;
import geoshot.bot.user.BotUserService;
import geoshot.contribution.tag.Tag;
import org.junit.Before;
import org.junit.Test;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class FindingClosestProcessor2Test {

    private BotUserService bus = mock(BotUserService.class);
    private ContributionService cs = mock(ContributionService.class);
    private Bot bot = mock(Bot.class);
    FindingClosestProcessor2 fcp = new FindingClosestProcessor2(bus, cs, bot);
    Update update = mock(Update.class);
    Message message = mock(Message.class);
    Location location = mock(Location.class);
    User user = mock(User.class);

    @Before
    public void setUp() {
        when(update.message()).thenReturn(message);
        when(message.location()).thenReturn(location);
        when(message.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        when(bus.targetTag(anyInt())).thenReturn(of(new Tag()));
        when(cs.getClosest(anyInt(), isA(Location.class))).thenReturn(of(new Contribution() {{ photo = new byte[]{}; }}));
    }

    @Test
    public void locationAndSearchingTagApplies() {
        assertTrue(fcp.appliesTo(update));
    }

    @Test
    public void notSearchingTagNotApplies() {
        when(bus.targetTag(anyInt())).thenReturn(empty());
        assertFalse(fcp.appliesTo(update));
    }

    @Test
    public void noLocationNotApplies() {
        when(message.location()).thenReturn(null);
        assertFalse(fcp.appliesTo(update));
    }

    @Test
    public void sendingLocationAndPhoto() {
        fcp.process(update);
        verify(cs).getClosest(anyInt(), isA(Location.class));
        verify(bus).dismissSearchingForTag(anyInt());
        verify(bot).execute(isA(SendLocation.class));
        verify(bot).execute(isA(SendPhoto.class));
    }

}