package geoshot.contribution;

import com.pengrad.telegrambot.model.Location;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import geoshot.bot.user.BotUserService;
import org.junit.Before;
import org.junit.Test;

import static geoshot.bot.user.BotUser.Action.CONTRIBUTING1;
import static geoshot.bot.user.BotUser.Action.CONTRIBUTING2;
import static geoshot.bot.user.BotUser.Action.NONE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class ContributingProcessor2Test {

    ContributionService cs = mock(ContributionService.class);
    BotUserService bus = mock(BotUserService.class);
    Bot bot = mock(Bot.class);
    ContributingProcessor2 cp = new ContributingProcessor2(cs, bus, bot);
    Update update = mock(Update.class);
    private Message message = mock(Message.class);
    private Location location = mock(Location.class);
    private User user = mock(User.class);

    @Before
    public void setUp() {
        when(update.message()).thenReturn(message);
    }

    @Test
    public void locationContributing1Applies() {
        when(bus.getActionById(anyInt())).thenReturn(CONTRIBUTING1);
        when(message.location()).thenReturn(location);
        when(message.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        assertTrue(cp.appliesTo(update));
    }

    @Test
    public void noLocationNotApplies() {
        when(message.location()).thenReturn(null);
        assertFalse(cp.appliesTo(update));
    }

    @Test
    public void locationNotContributing1NotApplies() {
        when(bus.getActionById(anyInt())).thenReturn(NONE);
        when(message.location()).thenReturn(location);
        when(message.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        assertFalse(cp.appliesTo(update));
    }

    @Test
    public void creatingContribution() {
        int id = 1;
        User user = mock(User.class);
        when(message.from()).thenReturn(user);
        when(user.id()).thenReturn(id);
        when(message.location()).thenReturn(location);
        cp.process(update);
        verify(cs).createContributionWithLocation(id, location);
        verify(bus).updateAction(id, CONTRIBUTING2);
        verify(bot).execute(isA(SendMessage.class));
    }

}