package geoshot.contribution;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.user.BotUserService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import static geoshot.bot.user.BotUser.Action.CONTRIBUTING1;
import static org.mockito.Mockito.*;

public class ContributingProcessor1Test {

    BotUserService service = mock(BotUserService.class);
    Bot bot = mock(Bot.class);
    ContributingProcessor1 cp = new ContributingProcessor1(service, bot);
    Update update = mock(Update.class);
    private CallbackQuery callbackQuery = mock(CallbackQuery.class);
    private User user = mock(User.class);
    private Message message = mock(Message.class);

    @Test
    public void editingMessage() {
        int id = 1;
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.from()).thenReturn(user);
        when(user.id()).thenReturn(id);
        when(callbackQuery.message()).thenReturn(message);
        when(message.messageId()).thenReturn(1);
        cp.process(update);
        verify(service).updateAction(id, CONTRIBUTING1);
        verify(bot).execute(ArgumentMatchers.isA(EditMessageText.class));
    }

}