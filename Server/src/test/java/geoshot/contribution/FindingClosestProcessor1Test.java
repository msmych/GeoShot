package geoshot.contribution;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.user.BotUserService;
import geoshot.contribution.tag.TagService;
import org.junit.Before;
import org.junit.Test;

import static geoshot.bot.user.BotUser.Action.FINDING_TAG;
import static geoshot.bot.user.BotUser.Action.NONE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class FindingClosestProcessor1Test {

    private TagService ts = mock(TagService.class);
    private BotUserService bus = mock(BotUserService.class);
    private Bot bot = mock(Bot.class);
    FindingClosestProcessor1 fcp = new FindingClosestProcessor1(ts, bus, bot);
    Update update = mock(Update.class);
    CallbackQuery callbackQuery = mock(CallbackQuery.class);
    User user = mock(User.class);
    Message message = mock(Message.class);

    @Before
    public void setUp() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        when(callbackQuery.data()).thenReturn("1");
        when(callbackQuery.message()).thenReturn(message);
        when(message.messageId()).thenReturn(1);
        when(bus.getActionById(anyInt())).thenReturn(FINDING_TAG);
        when(ts.isTagId(isA(Update.class))).thenReturn(true);
    }

    @Test
    public void findingTagIsTagIdApplies() {
        assertTrue(fcp.appliesTo(update));
    }

    @Test
    public void isNotTagIdNotApplies() {
        when(ts.isTagId(isA(Update.class))).thenReturn(false);
        assertFalse(fcp.appliesTo(update));
    }

    @Test
    public void notFindingTagNotApplies() {
        when(bus.getActionById(anyInt())).thenReturn(NONE);
        assertFalse(fcp.appliesTo(update));
    }

    @Test
    public void editingMessage() {
        fcp.process(update);
        verify(bus).updateTargetTag(anyInt(), anyInt());
        verify(bot).execute(isA(EditMessageText.class));
    }

}