package geoshot.contribution;

import com.pengrad.telegrambot.model.*;
import com.pengrad.telegrambot.request.GetFile;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.GetFileResponse;
import geoshot.bot.Bot;
import geoshot.contribution.tag.TagService;
import geoshot.bot.user.BotUserService;
import org.junit.Before;
import org.junit.Test;

import static geoshot.bot.user.BotUser.Action.CONTRIBUTING2;
import static geoshot.bot.user.BotUser.Action.CONTRIBUTING3;
import static geoshot.bot.user.BotUser.Action.NONE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class ContributingProcessor3Test {

    private BotUserService bus = mock(BotUserService.class);
    private ContributionService cs = mock(ContributionService.class);
    TagService ts = mock(TagService.class);
    private Bot bot = mock(Bot.class);
    ContributingProcessor3 cp = new ContributingProcessor3(bus, cs, ts, bot);
    Update update = mock(Update.class);
    Message message = mock(Message.class);
    User user = mock(User.class);

    @Before
    public void setUp() {
        when(update.message()).thenReturn(message);
    }

    @Test
    public void photoContributing2Applies() {
        when(message.photo()).thenReturn(new PhotoSize[]{});
        when(message.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        when(bus.getActionById(anyInt())).thenReturn(CONTRIBUTING2);
        assertTrue(cp.appliesTo(update));
    }

    @Test
    public void photoNotContributing2NotApplies() {
        when(message.photo()).thenReturn(new PhotoSize[]{});
        when(message.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        when(bus.getActionById(anyInt())).thenReturn(NONE);
        assertFalse(cp.appliesTo(update));
    }

    @Test
    public void noPhotoNotApplies() {
        when(message.photo()).thenReturn(null);
        assertFalse(cp.appliesTo(update));
    }

    @Test
    public void savingPhotoAndSendingMessage() {
        PhotoSize photoSize = mock(PhotoSize.class);
        when(message.photo()).thenReturn(new PhotoSize[]{photoSize});
        when(photoSize.fileId()).thenReturn("file id");
        GetFileResponse getFileResponse = mock(GetFileResponse.class);
        File file = mock(File.class);
        when(getFileResponse.file()).thenReturn(file);
        when(bot.execute(isA(GetFile.class))).thenReturn(getFileResponse);
        when(bot.getFullFilePath(file)).thenReturn("full file path");
        when(message.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        cp.process(update);
        verify(bot).execute(isA(GetFile.class));
        verify(bot).getFullFilePath(isA(File.class));
        verify(cs).savePhoto(anyInt(), anyString());
        verify(bus).updateAction(1, CONTRIBUTING3);
        verify(ts).getAllTagsKeyboard();
        verify(bot).execute(isA(SendMessage.class));
    }

}