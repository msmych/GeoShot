package geoshot.contribution;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.contribution.tag.TagService;
import geoshot.bot.user.BotUserService;
import org.junit.Before;
import org.junit.Test;

import static geoshot.bot.user.BotUser.Action.CONTRIBUTING3;
import static geoshot.bot.user.BotUser.Action.NONE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ContributingProcessor4Test {

    private BotUserService bus = mock(BotUserService.class);
    private TagService ts = mock(TagService.class);
    ContributionService cs = mock(ContributionService.class);
    private Bot bot = mock(Bot.class);
    ContributingProcessor4 cp = new ContributingProcessor4(bus, ts, cs, bot);
    Update update = mock(Update.class);
    CallbackQuery callbackQuery = mock(CallbackQuery.class);
    Message message = mock(Message.class);
    User user = mock(User.class);

    @Before
    public void setUp() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        when(callbackQuery.message()).thenReturn(message);
        when(message.messageId()).thenReturn(1);
        when(callbackQuery.data()).thenReturn("1");
        when(bus.getActionById(anyInt())).thenReturn(CONTRIBUTING3);
        when(ts.isTagId(isA(Update.class))).thenReturn(true);
    }

    @Test
    public void contributing3AndIdTagIdApplies() {
        assertTrue(cp.appliesTo(update));
    }

    @Test
    public void contributing3NotTagIdNotApplies() {
        when(ts.isTagId(isA(Update.class))).thenReturn(false);
        assertFalse(cp.appliesTo(update));
    }

    @Test
    public void notContributing3NotApplies() {
        when(bus.getActionById(anyInt())).thenReturn(NONE);
        assertFalse(cp.appliesTo(update));
    }

    @Test
    public void savingContributionTagAndEditingMessage() {
        cp.process(update);
        verify(cs).updateTag(anyInt(), anyInt());
        verify(cs).submit(anyInt());
        verify(bus).updateAction(1, NONE);
        verify(bot).execute(isA(EditMessageText.class));
    }
}