package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class TagServiceTest {

    private TagRepository tr = mock(TagRepository.class);
    List<String> initialTags = Arrays.asList("tag1", "tag2");
    TagService ts = new TagService(tr, initialTags);
    Update update = mock(Update.class);
    CallbackQuery callbackQuery = mock(CallbackQuery.class);

    @Test
    public void initializing() {
        ts.init();
        verify(tr, times(initialTags.size())).save(isA(Tag.class));
    }

    @Test
    public void convertingTagsToInlineKeyboard() {
        Tag tag = mock(Tag.class);
        List<Tag> tags = Arrays.asList(tag, tag, tag, tag);
        when(tr.findAll()).thenReturn(tags);
        List<InlineKeyboardButton[]> allTagsKeyboard = ts.getAllTagsKeyboard();
        assertEquals(tags.size(), allTagsKeyboard.size());
    }

    @Test
    public void integerCallbackQueryDataPresentIsTagId() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.data()).thenReturn("1");
        when(tr.findById(anyInt())).thenReturn(of(new Tag()));
        assertTrue(ts.isTagId(update));
    }

    @Test
    public void notPresentIsNotTagId() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.data()).thenReturn("1");
        when(tr.findById(anyInt())).thenReturn(empty());
        assertFalse(ts.isTagId(update));
    }

    @Test
    public void notIntegerCallbackQueryDataIsNotTagId() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.data()).thenReturn("wrong");
        assertFalse(ts.isTagId(update));
    }

    @Test
    public void noCallbackQueryDataIsNotTagId() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.data()).thenReturn(null);
        assertFalse(ts.isTagId(update));
    }

    @Test
    public void notCallbackQueryIsNotTagId() {
        when(update.callbackQuery()).thenReturn(null);
        assertFalse(ts.isTagId(update));
    }

}