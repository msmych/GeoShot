package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.user.BotUserService;
import org.junit.Before;
import org.junit.Test;

import static geoshot.bot.user.BotUser.Action.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class TagDetailsProcessorTest {

    TagService ts = mock(TagService.class);
    BotUserService bus = mock(BotUserService.class);
    Bot bot = mock(Bot.class);
    TagDetailsProcessor tdp = new TagDetailsProcessor(ts, bus, bot);
    Update update = mock(Update.class);
    CallbackQuery callbackQuery = mock(CallbackQuery.class);
    User user = mock(User.class);
    Message message = mock(Message.class);

    @Before
    public void setUp() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.data()).thenReturn("1");
        when(callbackQuery.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        when(callbackQuery.message()).thenReturn(message);
        when(message.messageId()).thenReturn(1);
        when(bus.getActionById(anyInt())).thenReturn(ALL_TAGS);
        when(bus.isModerator(anyInt())).thenReturn(true);
        when(ts.isTagId(isA(Update.class))).thenReturn(true);
        when(ts.getById(anyInt())).thenReturn(new Tag("tag"));
    }

    @Test
    public void allTagsAndModeratorAndIsTagIdApplies() {
        assertTrue(tdp.appliesTo(update));
    }

    @Test
    public void notTagIdNotApplies() {
        when(ts.isTagId(isA(Update.class))).thenReturn(false);
        assertFalse(tdp.appliesTo(update));
    }

    @Test
    public void notModeratorNotApplies() {
        when(bus.isModerator(anyInt())).thenReturn(false);
        assertFalse(tdp.appliesTo(update));
    }

    @Test
    public void notAllTagsNotApplies() {
        when(bus.getActionById(anyInt())).thenReturn(NONE);
        assertFalse(tdp.appliesTo(update));
    }

    @Test
    public void editingMessage() {
        tdp.process(update);
        verify(bus).updateAction(1, TAG_DETAILS);
        verify(ts).getById(anyInt());
        verify(bot).execute(isA(EditMessageText.class));
    }
}