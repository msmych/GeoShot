package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.user.BotUserService;
import org.junit.Test;

import static geoshot.bot.user.BotUser.Action.FINDING_TAG;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class FindingTagProcessorTest {

    private TagService ts = mock(TagService.class);
    BotUserService bus = mock(BotUserService.class);
    private Bot bot = mock(Bot.class);
    FindingTagProcessor ftp = new FindingTagProcessor(ts, bus, bot);

    @Test
    public void editingMessage() {
        Update update = mock(Update.class);
        CallbackQuery callbackQuery = mock(CallbackQuery.class);
        Message message = mock(Message.class);
        User user = mock(User.class);
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.message()).thenReturn(message);
        when(message.messageId()).thenReturn(1);
        when(callbackQuery.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        ftp.process(update);
        verify(ts).getAllTagsKeyboard();
        verify(bus).updateAction(1, FINDING_TAG);
        verify(bot).execute(isA(EditMessageText.class));
    }

}