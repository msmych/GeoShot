package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import geoshot.bot.user.BotUserService;
import org.junit.Before;
import org.junit.Test;

import static geoshot.bot.user.BotUser.Action.ADDING_TAG1;
import static geoshot.bot.user.BotUser.Action.ALL_TAGS;
import static geoshot.bot.user.BotUser.Action.NONE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AddingTagProcessor2Test {

    private TagService ts = mock(TagService.class);
    BotUserService bus = mock(BotUserService.class);
    private Bot bot = mock(Bot.class);
    AddingTagProcessor2 tap = new AddingTagProcessor2(ts, bus, bot);
    Update update = mock(Update.class);
    Message message = mock(Message.class);
    User user = mock(User.class);

    @Before
    public void setUp() {
        when(update.message()).thenReturn(message);
        when(message.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
    }

    @Test
    public void messageTextAddingTag1Applies() {
        when(message.text()).thenReturn("text");
        when(bus.getActionById(anyInt())).thenReturn(ADDING_TAG1);
        assertTrue(tap.appliesTo(update));
    }

    @Test
    public void messageTextNotAddingTag1NotApplies() {
        when(message.text()).thenReturn("text");
        when(bus.getActionById(anyInt())).thenReturn(NONE);
        assertFalse(tap.appliesTo(update));
    }

    @Test
    public void noMessageTextNotApplies() {
        when(message.text()).thenReturn(null);
        assertFalse(tap.appliesTo(update));
    }

    @Test
    public void savingTagAndSendingMessage() {
        when(message.text()).thenReturn("text");
        tap.process(update);
        verify(ts).save(anyString());
        verify(bus).updateAction(1, ALL_TAGS);
        verify(ts).getAllTagsKeyboard();
        verify(bot).execute(isA(SendMessage.class));
    }
}