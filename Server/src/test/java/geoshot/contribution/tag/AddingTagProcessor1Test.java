package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.user.BotUserService;
import org.junit.Before;
import org.junit.Test;

import static geoshot.bot.Bot.ADD_DATA;
import static geoshot.bot.user.BotUser.Action.ADDING_TAG1;
import static geoshot.bot.user.BotUser.Action.ALL_TAGS;
import static geoshot.bot.user.BotUser.Action.NONE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class AddingTagProcessor1Test {

    private BotUserService bus = mock(BotUserService.class);
    private Bot bot = mock(Bot.class);
    AddingTagProcessor1 tap = new AddingTagProcessor1(bus, bot);

    Update update = mock(Update.class);
    CallbackQuery callbackQuery = mock(CallbackQuery.class);
    User user = mock(User.class);
    Message message = mock(Message.class);

    @Before
    public void setUp() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
    }

    @Test
    public void allTagsAddApplies() {
        when(bus.getActionById(anyInt())).thenReturn(ALL_TAGS);
        when(callbackQuery.data()).thenReturn(ADD_DATA);
        assertTrue(tap.appliesTo(update));
    }

    @Test
    public void allTagNotAddNotApplies() {
        when(bus.getActionById(anyInt())).thenReturn(ALL_TAGS);
        when(callbackQuery.data()).thenReturn("wrong");
        assertFalse(tap.appliesTo(update));
    }

    @Test
    public void notAllTagsNotApplies() {
        when(bus.getActionById(anyInt())).thenReturn(NONE);
        when(callbackQuery.data()).thenReturn(ADD_DATA);
        assertFalse(tap.appliesTo(update));
    }

    @Test
    public void editingMessage() {
        when(callbackQuery.message()).thenReturn(message);
        when(message.messageId()).thenReturn(1);
        tap.process(update);
        verify(bus).updateAction(1, ADDING_TAG1);
        verify(bot).execute(isA(EditMessageText.class));
    }

}