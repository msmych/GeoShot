package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.user.BotUserService;
import org.junit.Before;
import org.junit.Test;

import static geoshot.bot.user.BotUser.Action.NONE;
import static geoshot.bot.user.BotUser.Action.TAG_DETAILS;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RemovingTagProcessorTest {

    private TagService ts = mock(TagService.class);
    private BotUserService bus = mock(BotUserService.class);
    private Bot bot = mock(Bot.class);
    RemovingTagProcessor rtp = new RemovingTagProcessor(ts, bus, bot);
    Update update = mock(Update.class);
    CallbackQuery callbackQuery = mock(CallbackQuery.class);
    User user = mock(User.class);
    Message message = mock(Message.class);

    @Before
    public void setUp() {
        when(update.callbackQuery()).thenReturn(callbackQuery);
        when(callbackQuery.data()).thenReturn("remove");
        when(callbackQuery.from()).thenReturn(user);
        when(user.id()).thenReturn(1);
        when(callbackQuery.message()).thenReturn(message);
        when(message.messageId()).thenReturn(1);
        when(message.text()).thenReturn("tag");
        when(bus.getActionById(anyInt())).thenReturn(TAG_DETAILS);
    }

    @Test
    public void callbackQueryDataRemoveAndTagDetailsApplies() {
        assertTrue(rtp.appliesTo(update));
    }

    @Test
    public void callbackQueryDataRemoveNotTagDetailsNotApplies() {
        when(bus.getActionById(anyInt())).thenReturn(NONE);
        assertFalse(rtp.appliesTo(update));
    }

    @Test
    public void notCallbackQueryDataRemoveNotApplies() {
        when(callbackQuery.data()).thenReturn("wrong");
        assertFalse(rtp.appliesTo(update));
    }

    @Test
    public void noCallbackQueryData() {
        when(callbackQuery.data()).thenReturn(null);
        assertFalse(rtp.appliesTo(update));
    }

    @Test
    public void noCallbackQuery() {
        when(update.callbackQuery()).thenReturn(null);
        assertFalse(rtp.appliesTo(update));
    }

    @Test
    public void removingTagAndEditingMessage() {
        rtp.process(update);
        verify(ts).remove(anyString());
        verify(bus).updateAction(1, NONE);
        verify(bot).execute(isA(EditMessageText.class));
    }
}