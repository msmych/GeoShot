package geoshot.contribution;

import com.pengrad.telegrambot.model.Location;
import geoshot.contribution.tag.Tag;
import geoshot.contribution.tag.TagService;
import geoshot.bot.resource.IOService;
import org.junit.Test;

import static java.util.Optional.of;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class ContributionServiceTest {

    private ContributionRepository cr = mock(ContributionRepository.class);
    private IOService ios = mock(IOService.class);
    private TagService ts = mock(TagService.class);
    ContributionService cs = new ContributionService(cr, ios, ts);

    @Test
    public void cancellingSubmissionsByUserId() {
        cs.cancelSubmissionBy(anyInt());
        verify(cr).deleteByBotUserIdAndSubmittedFalse(anyInt());
    }

    @Test
    public void creatingContributionWithLocation() {
        Location location = mock(Location.class);
        when(location.latitude()).thenReturn(1f);
        when(location.longitude()).thenReturn(1f);
        cs.createContributionWithLocation(1, location);
        verify(cr).save(isA(Contribution.class));
    }

    @Test
    public void savingPhoto() {
        when(ios.loadImageFromFullPath(anyString())).thenReturn(of(new byte[]{}));
        when(cr.findOneByBotUserIdAndSubmittedFalse(anyInt())).thenReturn(of(new Contribution()));
        cs.savePhoto(1, "full file path");
        verify(ios).loadImageFromFullPath(anyString());
        verify(cr).findOneByBotUserIdAndSubmittedFalse(1);
        verify(cr).save(isA(Contribution.class));
    }

    @Test
    public void updatingTag() {
        when(cr.findOneByBotUserIdAndSubmittedFalse(anyInt())).thenReturn(of(new Contribution()));
        when(ts.getById(anyInt())).thenReturn(new Tag());
        cs.updateTag(1, 1);
        verify(cr).findOneByBotUserIdAndSubmittedFalse(anyInt());
        verify(ts).getById(anyInt());
        verify(cr).save(isA(Contribution.class));
    }

    @Test
    public void submitting() {
        when(cr.findOneByBotUserIdAndSubmittedFalse(anyInt())).thenReturn(of(new Contribution()));
        cs.submit(1);
        verify(cr).findOneByBotUserIdAndSubmittedFalse(anyInt());
        verify(cr).save(isA(Contribution.class));
    }

    @Test
    public void gettingAll() {
        cs.getByTagName("All");
        verify(cr).findAll();
    }

    @Test
    public void gettingByTagName() {
        cs.getByTagName("Tag");
        verify(cr).findAllByTagName(anyString());
    }
}