package geoshot.bot.update;

import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Update;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class GeoShotUpdatesListener implements UpdatesListener {

    private final Logger logger = LogManager.getLogger(GeoShotUpdatesListener.class);

    private final Set<UpdatePreProcessor> preProcessors;
    private final Set<UpdateProcessor> processors;

    public GeoShotUpdatesListener(Set<UpdatePreProcessor> preProcessors, Set<UpdateProcessor> processors) {
        this.preProcessors = preProcessors;
        this.processors = processors;
    }

    @Override
    public int process(List<Update> updates) {
        updates.forEach(update -> {
            preProcessors.forEach(pp -> preProcess(pp, update));
            processors.stream()
                    .filter(p -> applies(p, update))
                    .forEach(p -> process(p, update));
        });
        return CONFIRMED_UPDATES_ALL;
    }

    private void preProcess(UpdatePreProcessor preProcessor, Update update) {
        try {
            preProcessor.preProcess(update);
        } catch (Exception e) {
            logger.error("Error while preprocessing", e);
        }
    }

    private boolean applies(UpdateProcessor processor, Update update) {
        try {
            return processor.appliesTo(update);
        } catch (Exception e) {
            logger.error("Error while checking update", e);
            return false;
        }
    }

    private void process(UpdateProcessor processor, Update update) {
        try {
            processor.process(update);
        } catch (Exception e) {
            logger.error("Error while processing update", e);
        }
    }

    public interface UpdatePreProcessor {
        void preProcess(Update update);
    }

    public interface UpdateProcessor {
        boolean appliesTo(Update update);
        void process(Update update);
    }
}
