package geoshot.bot.update;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;

public class UpdateUtils {

    public static boolean isCallbackQueryData(String data, Update update) {
        CallbackQuery callbackQuery = update.callbackQuery();
        if (callbackQuery == null) return false;
        String callbackQueryData = callbackQuery.data();
        if (callbackQueryData == null) return false;
        return callbackQueryData.equals(data);
    }

    public static boolean isLocation(Update update) {
        Message message = update.message();
        return message != null && message.location() != null;
    }
}
