package geoshot.bot.update;

import com.pengrad.telegrambot.model.Update;
import geoshot.bot.update.GeoShotUpdatesListener.UpdatePreProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class LoggingPreProcessor implements UpdatePreProcessor {

    private final Logger logger = LogManager.getLogger(LoggingPreProcessor.class);

    @Override
    public void preProcess(Update update) {
        logger.info(update);
    }
}
