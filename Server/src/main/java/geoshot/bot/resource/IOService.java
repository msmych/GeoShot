package geoshot.bot.resource;

import geoshot.bot.GeoShotBotException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Optional;

@Service
public class IOService {

    private final Logger logger = LogManager.getLogger(IOService.class);

    public String loadFrom(Resource resource) {
        StringBuilder message = new StringBuilder();
        try (InputStream inputStream = resource.getInputStream()) {
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.replace("  ", "\n");
                message.append(line);
            }
        } catch (Exception e) {
            logger.error("Error while loading resource", e);
        }
        return message.toString();
    }

    public Optional<byte[]> loadImageFromFullPath(String fullFilePath) {
        try {
            URL url = new URL(fullFilePath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(ImageIO.read(url), "JPG", baos);
            baos.flush();
            byte[] image = baos.toByteArray();
            baos.close();
            return Optional.of(image);
        } catch (java.io.IOException e) {
            logger.error(new GeoShotBotException(), e);
        }
        return Optional.empty();
    }

}
