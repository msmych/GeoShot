package geoshot.bot.user;

import com.pengrad.telegrambot.model.Contact;
import com.pengrad.telegrambot.model.User;
import geoshot.contribution.tag.Tag;

import javax.persistence.*;

import static geoshot.bot.user.BotUser.Action.NONE;
import static geoshot.bot.user.BotUser.Role.CONTRIBUTOR;
import static geoshot.bot.user.BotUser.Role.MODERATOR;
import static javax.persistence.EnumType.STRING;

@Entity
public class BotUser {

    @Id
    public int id;

    @Enumerated(STRING)
    public Role role = CONTRIBUTOR;

    @Enumerated(STRING)
    public Action action = NONE;

    @ManyToOne @JoinColumn
    public Tag targetTag;

    public BotUser() {}

    BotUser(User user) {
        id = user.id();
    }

    BotUser(Contact contact) {
        id = contact.userId();
        role = MODERATOR;
    }

    public enum Role {CONTRIBUTOR, MODERATOR}

    public enum Action {
        NONE,
        CONTRIBUTING1, CONTRIBUTING2, CONTRIBUTING3,
        ALL_TAGS, ADDING_TAG1, TAG_DETAILS, FINDING_TAG
    }

}
