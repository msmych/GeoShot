package geoshot.bot.user;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import geoshot.bot.update.GeoShotUpdatesListener.UpdatePreProcessor;
import org.springframework.stereotype.Component;

@Component
public class BotUserSavingPreProcessor implements UpdatePreProcessor {

    private final BotUserService botUserService;

    public BotUserSavingPreProcessor(BotUserService botUserService) {
        this.botUserService = botUserService;
    }

    @Override
    public void preProcess(Update update) {
        Message message = update.message();
        if (message == null) return;
        botUserService.save(message.from());
    }
}
