package geoshot.bot.user;

import geoshot.bot.user.BotUser.Action;
import geoshot.bot.user.BotUser.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface BotUserRepository extends CrudRepository<BotUser, Integer> {

    @Query("select action from BotUser where id = :id")
    Optional<Action> findActionById(@Param("id") int id);

    @Query("select role from BotUser where id = :id")
    Optional<Role> findRoleById(@Param("id") int id);
}
