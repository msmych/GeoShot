package geoshot.bot.user;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import org.springframework.stereotype.Component;

@Component
public class ModeratorSavingProcessor implements UpdateProcessor {

    private final BotUserService botUserService;
    private final Bot bot;

    public ModeratorSavingProcessor(BotUserService botUserService, Bot bot) {
        this.botUserService = botUserService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        Message message = update.message();
        if (message == null || message.contact() == null) return false;
        return botUserService.isModerator(message.from().id());
    }

    @Override
    public void process(Update update) {
        botUserService.save(update.message().contact());
        bot.execute(new SendMessage(update.message().from().id(), "Saved as moderator"));
    }
}
