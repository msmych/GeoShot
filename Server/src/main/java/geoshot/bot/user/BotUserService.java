package geoshot.bot.user;

import com.pengrad.telegrambot.model.Contact;
import com.pengrad.telegrambot.model.User;
import geoshot.bot.GeoShotBotException;
import geoshot.bot.user.BotUser.Action;
import geoshot.contribution.tag.Tag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

import static geoshot.bot.user.BotUser.Role.MODERATOR;

@Service
public class BotUserService {

    private final BotUserRepository botUserRepository;
    private final List<String> moderators;

    public BotUserService(BotUserRepository botUserRepository,
                          @Value("${geoshot-bot.moderators}") List<String> moderators) {
        this.botUserRepository = botUserRepository;
        this.moderators = moderators;
    }

    @PostConstruct void init() {
        moderators.stream()
                .map(Integer::valueOf)
                .map(id -> {
                    BotUser botUser = new BotUser();
                    botUser.id = id;
                    botUser.role = MODERATOR;
                    return botUser;
                })
                .forEach(botUserRepository::save);
    }

    public BotUser save(User user) {
        return botUserRepository.save(
                botUserRepository.findById(user.id()).orElse(new BotUser(user)));
    }

    public BotUser save(Contact contact) {
        BotUser botUser = botUserRepository.findById(contact.userId()).orElse(new BotUser(contact));
        botUser.role = MODERATOR;
        return botUserRepository.save(botUser);
    }

    public Action getActionById(int botUserId) {
        return botUserRepository.findActionById(botUserId)
                .orElseThrow(GeoShotBotException::new);
    }

    public void updateAction(int botUserId, Action action) {
        BotUser botUser = botUserRepository.findById(botUserId)
                .orElseThrow(GeoShotBotException::new);
        botUser.action = action;
        botUserRepository.save(botUser);
    }

    public boolean isModerator(int botUserId) {
        return botUserRepository.findRoleById(botUserId)
                .orElseThrow(GeoShotBotException::new)
                == MODERATOR;
    }

    public void updateTargetTag(int botUserId, int tagId) {
        BotUser botUser = botUserRepository.findById(botUserId)
                .orElseThrow(GeoShotBotException::new);
        botUser.targetTag = new Tag() {{ id = tagId; }};
        botUserRepository.save(botUser);
    }

    public Optional<Tag> targetTag(int botUserId) {
        return botUserRepository.findById(botUserId)
                .map(botUser -> botUser.targetTag);
    }

    public void dismissSearchingForTag(int botUserId) {
        BotUser botUser = botUserRepository.findById(botUserId)
                .orElseThrow(GeoShotBotException::new);
        botUser.targetTag = null;
        botUserRepository.save(botUser);
    }
}
