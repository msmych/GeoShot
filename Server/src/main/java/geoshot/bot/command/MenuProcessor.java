package geoshot.bot.command;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.contribution.ContributionService;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import static geoshot.bot.Bot.MAIN_MENU;
import static geoshot.bot.Bot.MAIN_MENU_MARKUP;
import static geoshot.bot.Bot.MENU_DATA;
import static geoshot.bot.update.UpdateUtils.isCallbackQueryData;
import static geoshot.bot.user.BotUser.Action.NONE;

@Component
public class MenuProcessor implements UpdateProcessor {

    private final BotUserService botUserService;
    private final ContributionService contributionService;
    private final Bot bot;

    public MenuProcessor(BotUserService botUserService, ContributionService contributionService, Bot bot) {
        this.botUserService = botUserService;
        this.contributionService = contributionService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        return isCallbackQueryData(MENU_DATA, update);
    }

    @Override
    public void process(Update update) {
        int botUserId = update.callbackQuery().from().id();
        botUserService.updateAction(botUserId, NONE);
        contributionService.cancelSubmissionBy(botUserId);
        bot.execute(new EditMessageText(
                botUserId,
                update.callbackQuery().message().messageId(),
                MAIN_MENU)
                .replyMarkup(MAIN_MENU_MARKUP));
    }
}
