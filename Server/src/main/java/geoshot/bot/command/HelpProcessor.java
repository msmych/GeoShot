package geoshot.bot.command;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import geoshot.bot.resource.IOService;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import static com.pengrad.telegrambot.model.request.ParseMode.Markdown;

@Component
public class HelpProcessor implements UpdateProcessor {

    private final Bot bot;
    private final String help;

    public HelpProcessor(Bot bot,
                         @Value("classpath:message/help.md") Resource help,
                         IOService IOService) {
        this.bot = bot;
        this.help = IOService.loadFrom(help);
    }

    @Override
    public boolean appliesTo(Update update) {
        Message message = update.message();
        if (message == null) return false;
        String text = message.text();
        if (text == null) return false;
        String help = "/help";
        return text.equals(help)
                || text.equals(help + "@" + bot.getUser().username());
    }

    @Override
    public void process(Update update) {
        bot.execute(new SendMessage(update.message().chat().id(), help).parseMode(Markdown));
    }
}
