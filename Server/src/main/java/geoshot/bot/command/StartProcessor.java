package geoshot.bot.command;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import org.springframework.stereotype.Component;

import static geoshot.bot.Bot.MAIN_MENU;
import static geoshot.bot.Bot.MAIN_MENU_MARKUP;

@Component
public class StartProcessor implements UpdateProcessor {

    private final Bot bot;

    public StartProcessor(Bot bot) {
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        Message message = update.message();
        if (message == null) return false;
        String text = message.text();
        if (text == null) return false;
        String start = "/start";
        return text.equals(start)
                || text.equals(start + "@" + bot.getUser().username());
    }

    @Override
    public void process(Update update) {
        bot.execute(new SendMessage(update.message().chat().id(), MAIN_MENU)
                .replyMarkup(MAIN_MENU_MARKUP));
    }
}
