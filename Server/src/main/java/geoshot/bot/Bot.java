package geoshot.bot;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.request.GetMe;
import com.pengrad.telegrambot.response.BaseResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class Bot extends TelegramBot {

    public final static String TAGS = "Tags";

    public final static String CONTRIBUTE_DATA = "contribute";
    public final static String FIND_DATA = "find";
    public final static String TAGS_DATA = "tags";
    public final static String MENU_DATA = "menu";
    public final static String ADD_DATA = "add";
    public final static String REMOVE_DATA = "remove";

    public final static String MAIN_MENU = "Main menu";

    public final static InlineKeyboardButton CONTRIBUTE_BUTTON = new InlineKeyboardButton("Contribute")
            .callbackData(CONTRIBUTE_DATA);
    public final static InlineKeyboardButton FIND_BUTTON = new InlineKeyboardButton("Find")
            .callbackData(FIND_DATA);
    public final static InlineKeyboardButton TAGS_BUTTON = new InlineKeyboardButton(TAGS)
            .callbackData(TAGS_DATA);
    public final static InlineKeyboardButton CANCEL_BUTTON = new InlineKeyboardButton("« Cancel")
            .callbackData(MENU_DATA);
    public final static InlineKeyboardButton ADD_BUTTON = new InlineKeyboardButton("+ Add")
            .callbackData(ADD_DATA);
    public final static InlineKeyboardButton REMOVE_BUTTON = new InlineKeyboardButton("- Remove")
            .callbackData(REMOVE_DATA);
    public final static InlineKeyboardMarkup MAIN_MENU_MARKUP = new InlineKeyboardMarkup(
            new InlineKeyboardButton[] {CONTRIBUTE_BUTTON},
            new InlineKeyboardButton[] {FIND_BUTTON},
            new InlineKeyboardButton[] {TAGS_BUTTON});
    public final static InlineKeyboardMarkup CANCEL_MARKUP = new InlineKeyboardMarkup(
            new InlineKeyboardButton[]{CANCEL_BUTTON});
    public final static InlineKeyboardMarkup TAG_DETAILS_MARKUP = new InlineKeyboardMarkup(
            new InlineKeyboardButton[]{REMOVE_BUTTON},
            new InlineKeyboardButton[]{CANCEL_BUTTON});

    @Autowired private UpdatesListener updatesListener;
    private User user;

    public Bot(@Value("${geoshot-bot.token}") String token) {
        super(token);
    }

    @PostConstruct void init() {
        setUpdatesListener(updatesListener);
        user = execute(new GetMe()).user();
    }

    public User getUser() {
        return user;
    }

    @Aspect
    @Component
    public class BotAspect {

        private final Logger logger = LogManager.getLogger(BotAspect.class);

        @AfterReturning(
                pointcut = "execution(public * com.pengrad.telegrambot.TelegramBot.execute(..))",
                returning = "response")
        public void afterExecution(BaseResponse response) {
            if (response.isOk()) logger.info(response);
            else logger.error(response.description());
        }
    }
}
