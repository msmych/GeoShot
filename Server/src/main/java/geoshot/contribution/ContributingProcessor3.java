package geoshot.contribution;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.request.GetFile;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import geoshot.contribution.tag.TagService;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

import static geoshot.bot.Bot.CANCEL_BUTTON;
import static geoshot.bot.user.BotUser.Action.CONTRIBUTING2;
import static geoshot.bot.user.BotUser.Action.CONTRIBUTING3;

@Component
public class ContributingProcessor3 implements UpdateProcessor {

    private final BotUserService botUserService;
    private final ContributionService contributionService;
    private final TagService tagService;
    private final Bot bot;

    @Value("${geoshot-bot.photo-size}") private int photoSize;

    public ContributingProcessor3(BotUserService botUserService,
                                  ContributionService contributionService,
                                  TagService tagService,
                                  Bot bot) {
        this.botUserService = botUserService;
        this.contributionService = contributionService;
        this.tagService = tagService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        Message message = update.message();
        if (message == null || message.photo() == null) return false;
        return botUserService.getActionById(message.from().id()) == CONTRIBUTING2;
    }

    @Override
    public void process(Update update) {
        int botUserId = update.message().from().id();
        botUserService.updateAction(botUserId, CONTRIBUTING3);
        contributionService.savePhoto(
                botUserId,
                bot.getFullFilePath(bot.execute(
                        new GetFile(update.message().photo()[photoSize].fileId())).file()));
        bot.execute(new SendMessage(botUserId, "Select a tag")
                .replyMarkup(generateKeyboard()));
    }

    private InlineKeyboardMarkup generateKeyboard() {
        List<InlineKeyboardButton[]> ikbs = tagService.getAllTagsKeyboard();
        ikbs.add(new InlineKeyboardButton[]{CANCEL_BUTTON});
        return new InlineKeyboardMarkup(ikbs.toArray(new InlineKeyboardButton[][]{}));
    }
}
