package geoshot.contribution;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ContributionRepository extends JpaRepository<Contribution, Integer> {

    Optional<Contribution> findOneByBotUserIdAndSubmittedFalse(int botUserId);
    void deleteByBotUserIdAndSubmittedFalse(int botUserId);
    List<Contribution> findAllByTagName(String tagName);

    @Query(nativeQuery = true,
            value = "select * from contribution " +
                    "where tag_id in (select id from tag where id = :tagId) " +
                    "and id in (select id from (" +
                    "select id, min(sqrt(power(latitude - :latitude, 2) + power(longitude - :longitude, 2)))" +
                    "from contribution group by id)) limit 1")
    Optional<Contribution> findClosest(@Param("tagId") int tagId,
                                       @Param("latitude") float latitude,
                                       @Param("longitude") float longitude);
}
