package geoshot.contribution;

import com.pengrad.telegrambot.model.Location;
import geoshot.bot.GeoShotBotException;
import geoshot.bot.resource.IOService;
import geoshot.contribution.tag.TagService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContributionService {

    private final ContributionRepository contributionRepository;
    private final IOService ioService;
    private final TagService tagService;

    public ContributionService(ContributionRepository contributionRepository,
                               IOService ioService,
                               TagService tagService) {
        this.contributionRepository = contributionRepository;
        this.ioService = ioService;
        this.tagService = tagService;
    }

    @Transactional
    public void cancelSubmissionBy(int botUserId) {
        contributionRepository.deleteByBotUserIdAndSubmittedFalse(botUserId);
    }

    public void createContributionWithLocation(int botUserId, Location location) {
        contributionRepository.save(new Contribution(botUserId, location));
    }

    public void savePhoto(int botUserId, String fullFilePath) {
        byte[] photo = ioService.loadImageFromFullPath(fullFilePath)
                .orElseThrow(GeoShotBotException::new);
        Contribution contribution = contributionRepository.findOneByBotUserIdAndSubmittedFalse(botUserId)
                .orElseThrow(GeoShotBotException::new);
        contribution.photo = photo;
        contributionRepository.save(contribution);
    }

    public void updateTag(int botUserId, int tagId) {
        Contribution contribution = contributionRepository.findOneByBotUserIdAndSubmittedFalse(botUserId)
                .orElseThrow(GeoShotBotException::new);
        contribution.tag = tagService.getById(tagId);
        contributionRepository.save(contribution);
    }

    public void submit(int botUserId) {
        Contribution contribution = contributionRepository.findOneByBotUserIdAndSubmittedFalse(botUserId)
                .orElseThrow(GeoShotBotException::new);
        contribution.submitted = true;
        contributionRepository.save(contribution);
    }

    public Optional<Contribution> getClosest(int tagId, Location location) {
        return contributionRepository.findClosest(tagId, location.latitude(), location.longitude());
    }

    public List<Contribution> getByTagName(String tagName) {
        return tagName.equals("All")
                ? contributionRepository.findAll() : contributionRepository.findAllByTagName(tagName);
    }

    public Contribution getById(int id) {
        return contributionRepository.findById(id)
                .orElse(Contribution.wrong());
    }

    @Transactional
    public void remove(int id) {
        contributionRepository.deleteById(id);
    }
}
