package geoshot.contribution;

import com.pengrad.telegrambot.model.Location;
import geoshot.contribution.tag.Tag;
import geoshot.bot.user.BotUser;

import javax.persistence.*;

@Entity
public class Contribution {

    @Id @GeneratedValue public int id;
    @ManyToOne @JoinColumn public BotUser botUser;
    @Column public boolean submitted = false;
    @Column public float latitude;
    @Column public float longitude;
    @Lob public byte[] photo;
    @ManyToOne @JoinColumn public Tag tag;

    public Contribution(){}

    public Contribution(int botUserId, Location location) {
        this.botUser = new BotUser() {{ id = botUserId; }};
        this.latitude = location.latitude();
        this.longitude = location.longitude();
    }

    public static Contribution wrong() {
        Contribution contribution = new Contribution();
        contribution.tag = Tag.wrong();
        return contribution;
    }
}
