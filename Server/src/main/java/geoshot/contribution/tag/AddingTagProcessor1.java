package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import static geoshot.bot.Bot.ADD_DATA;
import static geoshot.bot.Bot.CANCEL_MARKUP;
import static geoshot.bot.update.UpdateUtils.isCallbackQueryData;
import static geoshot.bot.user.BotUser.Action.ADDING_TAG1;
import static geoshot.bot.user.BotUser.Action.ALL_TAGS;

@Component
public class AddingTagProcessor1 implements UpdateProcessor {

    private final BotUserService botUserService;
    private final Bot bot;

    public AddingTagProcessor1(BotUserService botUserService, Bot bot) {
        this.botUserService = botUserService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        if (!isCallbackQueryData(ADD_DATA, update)) return false;
        return botUserService.getActionById(update.callbackQuery().from().id()) == ALL_TAGS;
    }

    @Override
    public void process(Update update) {
        int botUserId = update.callbackQuery().from().id();
        botUserService.updateAction(botUserId, ADDING_TAG1);
        bot.execute(new EditMessageText(
                botUserId,
                update.callbackQuery().message().messageId(),
                "Please send new tag's name")
                .replyMarkup(CANCEL_MARKUP));
    }
}
