package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import java.util.List;

import static geoshot.bot.Bot.CANCEL_BUTTON;
import static geoshot.bot.Bot.FIND_DATA;
import static geoshot.bot.update.UpdateUtils.isCallbackQueryData;
import static geoshot.bot.user.BotUser.Action.FINDING_TAG;

@Component
public class FindingTagProcessor implements UpdateProcessor {

    private final TagService tagService;
    private final BotUserService botUserService;
    private final Bot bot;

    public FindingTagProcessor(TagService tagService, BotUserService botUserService, Bot bot) {
        this.tagService = tagService;
        this.botUserService = botUserService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        return isCallbackQueryData(FIND_DATA, update);
    }

    @Override
    public void process(Update update) {
        int botUserId = update.callbackQuery().from().id();
        botUserService.updateAction(botUserId, FINDING_TAG);
        bot.execute(new EditMessageText(
                botUserId,
                update.callbackQuery().message().messageId(),
                "Select a tag:")
                .replyMarkup(generateKeyboard()));
    }

    private InlineKeyboardMarkup generateKeyboard() {
        List<InlineKeyboardButton[]> allTagsKeyboard = tagService.getAllTagsKeyboard();
        allTagsKeyboard.add(new InlineKeyboardButton[]{CANCEL_BUTTON});
        return new InlineKeyboardMarkup(allTagsKeyboard.toArray(new InlineKeyboardButton[][]{}));
    }
}
