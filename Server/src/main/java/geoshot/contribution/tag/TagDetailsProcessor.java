package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import static geoshot.bot.Bot.TAG_DETAILS_MARKUP;
import static geoshot.bot.user.BotUser.Action.ALL_TAGS;
import static geoshot.bot.user.BotUser.Action.TAG_DETAILS;
import static java.lang.Integer.parseInt;

@Component
public class TagDetailsProcessor implements UpdateProcessor {

    private final TagService tagService;
    private final BotUserService botUserService;
    private final Bot bot;

    public TagDetailsProcessor(TagService tagService, BotUserService botUserService, Bot bot) {
        this.tagService = tagService;
        this.botUserService = botUserService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        if (!tagService.isTagId(update)) return false;
        int botUserId = update.callbackQuery().from().id();
        return botUserService.getActionById(botUserId) == ALL_TAGS
                && botUserService.isModerator(botUserId);
    }

    @Override
    public void process(Update update) {
        int botUserId = update.callbackQuery().from().id();
        botUserService.updateAction(botUserId, TAG_DETAILS);
        bot.execute(new EditMessageText(
                botUserId,
                update.callbackQuery().message().messageId(),
                tagService.getById(parseInt(update.callbackQuery().data())).name)
                .replyMarkup(TAG_DETAILS_MARKUP));
    }
}
