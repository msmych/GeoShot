package geoshot.contribution.tag;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Tag {

    @Id @GeneratedValue public int id;
    @Column public String name;

    public Tag() {}

    public Tag(String name) {
        this.name = name;
    }

    public static Tag wrong() {
        Tag tag = new Tag();
        tag.name = "WRONG";
        return tag;
    }
}
