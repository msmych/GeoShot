package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import java.util.List;

import static geoshot.bot.Bot.*;
import static geoshot.bot.update.UpdateUtils.isCallbackQueryData;
import static geoshot.bot.user.BotUser.Action.ALL_TAGS;

@Component
public class AllTagsProcessor implements UpdateProcessor {

    private final TagService tagService;
    private final BotUserService botUserService;
    private final Bot bot;

    public AllTagsProcessor(TagService tagService, BotUserService botUserService, Bot bot) {
        this.tagService = tagService;
        this.botUserService = botUserService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        return isCallbackQueryData(TAGS_DATA, update);
    }

    @Override
    public void process(Update update) {
        int botUserId = update.callbackQuery().from().id();
        botUserService.updateAction(botUserId, ALL_TAGS);
        bot.execute(new EditMessageText(
                botUserId,
                update.callbackQuery().message().messageId(),
                TAGS)
                .replyMarkup(generateKeyboard(botUserId)));
    }

    private InlineKeyboardMarkup generateKeyboard(int botUserId) {
        List<InlineKeyboardButton[]> ikbs = tagService.getAllTagsKeyboard();
        if (botUserService.isModerator(botUserId))
            ikbs.add(new InlineKeyboardButton[]{ADD_BUTTON});
        ikbs.add(new InlineKeyboardButton[]{CANCEL_BUTTON});
        return new InlineKeyboardMarkup(ikbs.toArray(new InlineKeyboardButton[][]{}));
    }
}
