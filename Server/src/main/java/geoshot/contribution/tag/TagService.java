package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import geoshot.bot.GeoShotBotException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;
import static java.util.regex.Pattern.matches;

@Service
public class TagService {

    private final TagRepository tagRepository;
    private final List<String> initialTags;

    public TagService(TagRepository tagRepository,
                      @Value("${geoshot-bot.initial-tags}") List<String> initialTags) {
        this.tagRepository = tagRepository;
        this.initialTags = initialTags;
    }

    @PostConstruct void init() {
        initialTags.stream()
                .map(Tag::new)
                .forEach(tagRepository::save);
    }

    public Tag getById(int tagId) {
        return tagRepository.findById(tagId).orElseThrow(GeoShotBotException::new);
    }

    public boolean isTagId(Update update) {
        CallbackQuery callbackQuery = update.callbackQuery();
        if (callbackQuery == null) return false;
        String data = callbackQuery.data();
        if (data == null || !matches("[0-9]+", data)) return false;
        return tagRepository.findById(parseInt(data)).isPresent();
    }

    public List<InlineKeyboardButton[]> getAllTagsKeyboard() {
        return tagRepository.findAll().stream()
                .map(tag -> new InlineKeyboardButton[]{
                        new InlineKeyboardButton(tag.name).callbackData(Integer.toString(tag.id))})
                .collect(Collectors.toList());
    }

    public void save(String name) {
        tagRepository.save(new Tag(name));
    }

    @Transactional
    public void remove(String name) {
        tagRepository.deleteByName(name);
    }

    public List<Tag> getAll() {
        return tagRepository.findAll();
    }
}
