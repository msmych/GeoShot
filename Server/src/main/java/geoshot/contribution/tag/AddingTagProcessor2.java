package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import java.util.List;

import static geoshot.bot.Bot.ADD_BUTTON;
import static geoshot.bot.Bot.CANCEL_BUTTON;
import static geoshot.bot.Bot.TAGS;
import static geoshot.bot.user.BotUser.Action.ADDING_TAG1;
import static geoshot.bot.user.BotUser.Action.ALL_TAGS;

@Component
public class AddingTagProcessor2 implements UpdateProcessor {

    private final TagService tagService;
    private final BotUserService botUserService;
    private final Bot bot;

    public AddingTagProcessor2(TagService tagService, BotUserService botUserService, Bot bot) {
        this.tagService = tagService;
        this.botUserService = botUserService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        Message message = update.message();
        if (message == null || message.text() == null) return false;
        return botUserService.getActionById(message.from().id()) == ADDING_TAG1;
    }

    @Override
    public void process(Update update) {
        tagService.save(update.message().text());
        int botUserId = update.message().from().id();
        botUserService.updateAction(botUserId, ALL_TAGS);
        bot.execute(new SendMessage(botUserId, TAGS)
                .replyMarkup(generateKeyboard()));
    }

    private InlineKeyboardMarkup generateKeyboard() {
        List<InlineKeyboardButton[]> ikbs = tagService.getAllTagsKeyboard();
        ikbs.add(new InlineKeyboardButton[]{ADD_BUTTON});
        ikbs.add(new InlineKeyboardButton[]{CANCEL_BUTTON});
        return new InlineKeyboardMarkup(ikbs.toArray(new InlineKeyboardButton[][]{}));
    }
}
