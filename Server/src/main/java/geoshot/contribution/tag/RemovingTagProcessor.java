package geoshot.contribution.tag;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import static geoshot.bot.Bot.MAIN_MENU;
import static geoshot.bot.Bot.MAIN_MENU_MARKUP;
import static geoshot.bot.Bot.REMOVE_DATA;
import static geoshot.bot.update.UpdateUtils.isCallbackQueryData;
import static geoshot.bot.user.BotUser.Action.NONE;
import static geoshot.bot.user.BotUser.Action.TAG_DETAILS;

@Component
public class RemovingTagProcessor implements UpdateProcessor {

    private final TagService tagService;
    private final BotUserService botUserService;
    private final Bot bot;

    public RemovingTagProcessor(TagService tagService, BotUserService botUserService, Bot bot) {
        this.tagService = tagService;
        this.botUserService = botUserService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        return isCallbackQueryData(REMOVE_DATA, update)
                && botUserService.getActionById(update.callbackQuery().from().id()) == TAG_DETAILS;
    }

    @Override
    public void process(Update update) {
        tagService.remove(update.callbackQuery().message().text());
        int botUserId = update.callbackQuery().from().id();
        botUserService.updateAction(botUserId, NONE);
        bot.execute(new EditMessageText(
                botUserId,
                update.callbackQuery().message().messageId(),
                MAIN_MENU)
                .replyMarkup(MAIN_MENU_MARKUP));
    }
}
