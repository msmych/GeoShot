package geoshot.contribution;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("contribution")
public class ContributionController {

    private final ContributionService contributionService;

    public ContributionController(ContributionService contributionService) {
        this.contributionService = contributionService;
    }

    @GetMapping("{tagName}")
    public List<Contribution> getAllContributions(@PathVariable("tagName") String tagName) {
        return contributionService.getByTagName(tagName);
    }

    @GetMapping("{id}/details")
    public Contribution getDetails(@PathVariable("id") int id) {
        return contributionService.getById(id);
    }

    @DeleteMapping("{id}")
    public void remove(@PathVariable("id") int id) {
        contributionService.remove(id);
    }
}
