package geoshot.contribution;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.SendLocation;
import com.pengrad.telegrambot.request.SendPhoto;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import static geoshot.bot.update.UpdateUtils.isLocation;

@Component
public class FindingClosestProcessor2 implements UpdateProcessor {

    private final BotUserService botUserService;
    private final ContributionService contributionService;
    private final Bot bot;

    public FindingClosestProcessor2(BotUserService botUserService, ContributionService contributionService, Bot bot) {
        this.botUserService = botUserService;
        this.contributionService = contributionService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        return isLocation(update) && botUserService.targetTag(update.message().from().id()).isPresent();
    }

    @Override
    public void process(Update update) {
        int botUserId = update.message().from().id();
        Contribution contribution = contributionService.getClosest(
                botUserService.targetTag(botUserId).get().id, update.message().location()).get();
        botUserService.dismissSearchingForTag(botUserId);
        bot.execute(new SendLocation(botUserId, contribution.latitude, contribution.longitude));
        bot.execute(new SendPhoto(botUserId, contribution.photo));
    }
}
