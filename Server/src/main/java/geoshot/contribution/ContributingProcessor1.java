package geoshot.contribution;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import static geoshot.bot.Bot.CANCEL_MARKUP;
import static geoshot.bot.Bot.CONTRIBUTE_DATA;
import static geoshot.bot.update.UpdateUtils.isCallbackQueryData;
import static geoshot.bot.user.BotUser.Action.CONTRIBUTING1;

@Component
public class ContributingProcessor1 implements UpdateProcessor {

    private final BotUserService botUserService;
    private final Bot bot;

    public ContributingProcessor1(BotUserService botUserService, Bot bot) {
        this.botUserService = botUserService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        return isCallbackQueryData(CONTRIBUTE_DATA, update);
    }

    @Override
    public void process(Update update) {
        int userId = update.callbackQuery().from().id();
        botUserService.updateAction(userId, CONTRIBUTING1);
        bot.execute(new EditMessageText(
                userId,
                update.callbackQuery().message().messageId(),
                "Please send your location")
                        .replyMarkup(CANCEL_MARKUP));
    }
}
