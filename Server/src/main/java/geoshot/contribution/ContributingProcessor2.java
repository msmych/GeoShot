package geoshot.contribution;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.SendMessage;
import geoshot.bot.Bot;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import static geoshot.bot.Bot.CANCEL_MARKUP;
import static geoshot.bot.update.UpdateUtils.isLocation;
import static geoshot.bot.user.BotUser.Action.CONTRIBUTING1;
import static geoshot.bot.user.BotUser.Action.CONTRIBUTING2;

@Component
public class ContributingProcessor2 implements UpdateProcessor {

    private final ContributionService contributionService;
    private final BotUserService botUserService;
    private final Bot bot;

    public ContributingProcessor2(ContributionService contributionService, BotUserService botUserService, Bot bot) {
        this.contributionService = contributionService;
        this.botUserService = botUserService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        return isLocation(update) && botUserService.getActionById(update.message().from().id()) == CONTRIBUTING1;
    }

    @Override
    public void process(Update update) {
        int botUserId = update.message().from().id();
        contributionService.createContributionWithLocation(botUserId, update.message().location());
        botUserService.updateAction(botUserId, CONTRIBUTING2);
        bot.execute(new SendMessage(botUserId, "Please send a photo")
                .replyMarkup(CANCEL_MARKUP));
    }
}
