package geoshot.contribution;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.EditMessageText;
import geoshot.bot.Bot;
import geoshot.contribution.tag.TagService;
import geoshot.bot.update.GeoShotUpdatesListener.UpdateProcessor;
import geoshot.bot.user.BotUserService;
import org.springframework.stereotype.Component;

import static geoshot.bot.Bot.MAIN_MENU_MARKUP;
import static geoshot.bot.user.BotUser.Action.CONTRIBUTING3;
import static geoshot.bot.user.BotUser.Action.NONE;
import static java.lang.Integer.parseInt;

@Component
public class ContributingProcessor4 implements UpdateProcessor {

    private final BotUserService botUserService;
    private final TagService tagService;
    private final ContributionService contributionService;
    private final Bot bot;

    public ContributingProcessor4(BotUserService botUserService,
                                  TagService tagService,
                                  ContributionService contributionService,
                                  Bot bot) {
        this.botUserService = botUserService;
        this.tagService = tagService;
        this.contributionService = contributionService;
        this.bot = bot;
    }

    @Override
    public boolean appliesTo(Update update) {
        return tagService.isTagId(update)
                && botUserService.getActionById(update.callbackQuery().from().id()) == CONTRIBUTING3;
    }

    @Override
    public void process(Update update) {
        int botUserId = update.callbackQuery().from().id();
        contributionService.updateTag(botUserId, parseInt(update.callbackQuery().data()));
        contributionService.submit(botUserId);
        botUserService.updateAction(botUserId, NONE);
        bot.execute(new EditMessageText(
                botUserId,
                update.callbackQuery().message().messageId(),
                "Thank you for your submission")
                .replyMarkup(MAIN_MENU_MARKUP));
    }
}
